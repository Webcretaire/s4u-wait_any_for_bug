#include <simgrid/s4u.hpp>
#include <vector>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_wait_for_bug, "Messages specific for this example");

using namespace std;

static void getter(vector<string> args) {
    XBT_INFO("Let's work for a while");
    simgrid::s4u::this_actor::execute(45000);
    int size = 1;

    // ==========
    // If I remove this line, I don't have any bug, wether I use wait_any or wait_any_for, which makes no sense to me
    simgrid::s4u::Actor::self()->daemonize();
    // ==========

    int *event = new int;
    vector<simgrid::s4u::CommPtr> comms(size);

    XBT_INFO("I'll wait for a message");
    comms[0] = simgrid::s4u::Mailbox::by_name("fake_mailbox")->get_async((void **) &event);


    /* With this line, everything works fine : the daemon is killed and the simulation exits whithout errors */
//    simgrid::s4u::Comm::wait_any(&comms);


    /* But if we do this instead, at the end of the simulation we get :
     *
     * [99999.000450] simgrid_path/src/xbt/exception.cpp:44: [xbt_exception/CRITICAL] Uncaught exception std::bad_alloc: std::bad_alloc
     *
     * Which we shouldn't : the simulation should be stopped at time 0.000600, and nether get to 99999.000450
     * (And event if it did, wait_any_for is supposed to return a special value in case of timeout, without
     * throwing an error, I think)
     *
     * (complete output in out.txt) */
    simgrid::s4u::Comm::wait_any_for(&comms, 99999);
}

static void master(vector<string> args) {
    XBT_INFO("Let's work for a while");
    simgrid::s4u::this_actor::execute(60000);
    XBT_INFO("Everybody die now");
    // At this moment, everyone should be killed, and we should exit without error
}

int main(int argc, char *argv[]) {
    simgrid::s4u::Engine e(&argc, argv);
    xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

    e.register_function("master", &master);
    e.register_function("getter", &getter);

    e.load_platform(argv[1]);
    e.load_deployment(argv[2]);

    e.run();

    XBT_INFO("Simulation is over");

    return 0;
}
