# S4U wait_any_for bug

This repo demonstrates a behaviour I don't understand when using wait_any_for vs wait_any. It looks like it is related to [this issue](https://framagit.org/simgrid/simgrid/issues/48) in SimGrid

The problem is described in the comments of `wait_for_bug.cpp` : basically when using `wait_any_for` we have an `std::bad_alloc` at the end of the simulation, which we don't have when using `wait_any`

To reproduce, simply use 
```shell
cd build && \
    cmake .. && \
    make -j 2 && \
    ./current ../platform.xml ../deploy.xml --cfg=surf/precision:1e-9 
```

Or, if you trust me, an example output already exists in `out.txt` (error case)

## Additionnal important info

With this patch on SimGrid :

```patch
--- a/src/kernel/actor/ActorImpl.cpp
+++ b/src/kernel/actor/ActorImpl.cpp
@@ -430,7 +430,7 @@ void ActorImpl::throw_exception(std::exception_ptr e)
 
 void ActorImpl::simcall_answer()
 {
-  if (this != simix_global->maestro_) {
+  if (this != simix_global->maestro_ && simcall.call_ != SIMCALL_NONE) {
     XBT_DEBUG("Answer simcall %s (%d) issued by %s (%p)", SIMIX_simcall_name(simcall.call_), (int)simcall.call_,
               get_cname(), this);
     simcall.call_ = SIMCALL_NONE;
```

we don't have the `std::bad_alloc` error, but the daemon actor is not killed either : the simulation only exits after the `wait_any_for` timeout, which is not what we expect.

So with the patch, if we use `wait_any_for`, we have the same behaviour whether the actor is daemonized or not (i.e the simulation ends without error after the timeout), but if we use `wait_any` we get the correct expected behaviour (i.e the daemon actor is killed)